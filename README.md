Модуль IM(backend) для Yii2
=============================

Модуль Интернет магазина. Каталог, товары, заказы, цены, скидки, купоны.

Installation
------------

Добавление нового модуля

Add to composer.json:
```json
"emilasp/yii2-im-backend": "*"
```
AND
```json
"repositories": [
        {
            "type": "git",
            "url":  "https://bitbucket.org/emilasp/yii2-im-backend.git"
        }
    ]
```

to the require section of your `composer.json` file.

Configuring
--------------------------

Добавляем :

```php
'modules' => [
        'im' => []
    ],
```
